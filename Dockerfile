FROM python:3.9-slim-buster

ENV ST_URL=$ST_URL \
    ST_NAME=$ST_NAME \
    ST_TIMEOUT=$ST_TIMEOUT \
    TZ=$TZ \
    SSH_HOST=$SSH_HOST \
    SSH_USER=$SSH_USER \
    SSH_CMD=$SSH_CMD

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY simple-trigger.py /app/simple-trigger.py

CMD ["python3", "/app/simple-trigger.py"]