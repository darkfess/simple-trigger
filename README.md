# simple-trigger

Simple trigger - if targeted web-resource is unavailable it triggers remote host (ssh-key auth) with custom bash command/script.
<br><br>
See also: [Simple monitoring](https://gitlab.com/darkfess/simple-monitoring)
<br><br>

### Quickstart

    docker run -it -d --name simple-trigger --restart=always \
      -v /home/darkfess/.ssh/id_rsa:/app/ssh_key \
      -e ST_URL=https://darkfess.ru \
      -e ST_NAME=DARKFESS \
      -e ST_TIMEOUT=60 \
      -e SSH_HOST=192.168.1.2 \
      -e SSH_USER=root \
      -e SSH_CMD='uptime' \
      -e TZ=Europe/Moscow \
    darkfess/simple-trigger

<br>

### How it works
Every `ST_TIMEOUT` checkups monitors it's target `ST_URL` for web-resource availability:
- if check succeeded, nothing happens (just continue)
- if check failed, it's only 3 attempts (1 every `ST_TIMEOUT`*2) left until trigger starts `SSH_CMD` on `SSH_HOST` with `SSH_USER` privileges
  - after trigger worked, cycle continue from the beginning

<br><hr><br>

### Enviroment Variables
- ST_NAME
  - any name or id (example: DARKFESS)
- ST_URL
  - url of targeted web-resource (example: https://darkfess.ru)
- ST_TIMEOUT
  - timeout between requests, seconds (example: 60)
- SSH_HOST
  - remote ssh host ip to connect (example: 192.168.1.1)
- SSH_USER
  - remote host user to connect (example: root)
- SSH_CMD
  - custom command/script to execute (example: 'bash /home/root/some-script.sh')
- TZ
  - (optional) timezone for log timestamps (example: Europe/Moscow)
<br><br>

### Volumes
Prepare your ssh-key pair (private/public) before launch. Private part needs to be mounted as a volume. <br>
Public part needs to be located in `/YOUR-PATH/.ssh/authorized_keys` on remote host `SSH_HOST` in `SSH_USER` directory.
- /YOUR-PATH/.ssh/id_rsa:/app/ssh_key
  - your ssh-key (private)


<br><hr><br>

Docker Hub: [https://hub.docker.com/r/darkfess/simple-trigger](https://hub.docker.com/r/darkfess/simple-trigger)