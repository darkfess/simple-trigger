import os
import requests
import time
import datetime
import paramiko

monitoring_url = os.environ['ST_URL']
service_name = os.environ['ST_NAME']
timeout = os.environ['ST_TIMEOUT']
ssh_hostname = os.environ['SSH_HOST']
ssh_username = os.environ['SSH_USER']
ssh_cmd = os.environ['SSH_CMD']

### 3/3 stability-levels
checking = [True, True, True]

def trigger():
    while True:

## request for status
        status = requests.get(monitoring_url).status_code

### if [OK]
        if status == 200:
            print(datetime.datetime.now(), "| [ OK ]", service_name)

## +1 stability-level
            checking.append(True)
            del checking[0]
            # print(checking)
            # print(checking.count(True), "/ 3")

## current stability-level
            checkup = any(checking)
            # print(checkup)

## print info
            if checkup == True:
                print('[ STABLE ]', checking.count(True), "/ 3")
            elif checkup == False:
                print('[ UNSTABLE ]', checking.count(True), "/ 3")

### if [FAIL]
        elif status >= 400:
            print(datetime.datetime.now(), "| [ FAIL ]", service_name)

## -1 stability-level
            checking.append(False)
            del checking[0]
            # print(checking)
            # print(checking.count(True), "/ 3")

## current stability-level
            checkup = any(checking)
            # print(checkup)

## print info
            if checkup == True:
                print('[ UNSTABLE ]', checking.count(True), "/ 3")
            elif checkup == False:
                print('[ CRUSHED ]' , checking.count(True), "/ 3", '... now execute SSH-script')

## 0/3 stability-level (trigger activation): ssh-connection and command/script execution
                ssh = paramiko.SSHClient()
                sshkey = paramiko.RSAKey.from_private_key_file(filename='/app/ssh_key')
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.connect(hostname=ssh_hostname, username=ssh_username, pkey=sshkey)
                stdin, stdout, stderr = ssh.exec_command(ssh_cmd)
                print(stdout.readlines())

## +1 stability-level in the front of the queue (now we get timeout*3 time, until 0/3 level again)
                checking.append(True)
                del checking[0]

## loop
        time.sleep(int(timeout))

trigger()